class Car {
    static list = [];

    static init(cars) {
        this.list = cars.map((i) => new this(i));
    }

    constructor({
        id,
        plate,
        manufacture,
        model,
        image,
        rentPerDay,
        capacity,
        description,
        transmission,
        available,
        type,
        year,
        options,
        specs,
        availableAt,
    }) {
        this.id = id;
        this.plate = plate;
        this.manufacture = manufacture;
        this.model = model;
        this.image = image;
        this.rentPerDay = rentPerDay;
        this.capacity = capacity;
        this.description = description;
        this.transmission = transmission;
        this.available = available;
        this.type = type;
        this.year = year;
        this.options = options;
        this.specs = specs;
        this.availableAt = availableAt;
    }

    render() {
        return `
        <div class="carContainer mx-2 ms-4 align-items-center">
            <div class="card col-lg-4 mx-2 my-2" style="width: 18rem; height: 500px">
                <img src="${this.image}" class="card-img-top mt-2" alt="..." style="height: 190px; object-fit: scale-down;">
                <div class="card-body">
                    <p>${this.manufacture}/${this.model}</p>
                    <h5 class="price fw-bold">${rupiah(this.rentPerDay)} / hari</h5>
                    <p class="card-text fifty-chars" title="${this.description}">${this.description}</p>
                    <p><img src="../img/fi_users.png" alt="" class="poin mx-2">${this.capacity} Orang</p>
                    <p><img src="../img/fi_gear.png" alt="" class="poin mx-2">${this.transmission}</p>
                    <p><img src="../img/fi_calendar.png" alt="" class="poin mx-2">Tahun ${this.year}</p>
                    <a href="#" class="btn green-4 text-white fwbold mt-1 w-100">Pilih Mobil</a>
                </div>
            </div>
        </div>
      `;
    }
}

function rupiah(number) {
    return new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(number);
}
