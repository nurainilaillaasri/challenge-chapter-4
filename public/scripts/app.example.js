class App {
    constructor() {
        this.clearButton = document.getElementById("clear-btn");
        this.loadButton = document.getElementById("load-btn");
        this.carContainerElement = document.getElementById("cars-container");
    }

    async init() {
        await this.load();
    }

    run = () => {
        Car.list.forEach((car) => {
            const node = document.createElement("div");
            node.innerHTML = car.render();
            this.carContainerElement.appendChild(node);
        });
    };

    async load() {
        this.clear();
        const driver = document.getElementById("driver").value;
        const passenger = document.getElementById("passenger").value;
        const date = document.getElementById("date").value;
        const time = document.getElementById("time").value;
        const dateTime = date + time;

        const cars = await Binar.listCars();
        const dateNow = this.getDateTimeNow();

        const availableCar = cars.filter((car) => {
            return driver != "" && car.capacity >= passenger && car.available === true && parseInt(Date.parse(car.availableAt)) > parseInt(dateTime) && parseInt(Date.parse(dateNow)) <= parseInt(Date.parse(dateTime));
        });

        Car.init(availableCar);
    }

    clear = () => {
        let child = this.carContainerElement.firstElementChild;

        while (child) {
            child.remove();
            child = this.carContainerElement.firstElementChild;
        }
    };

    getDateTimeNow() {
        var today = new Date();
        var date = today.getFullYear() + '-' + String((today.getMonth() + 1)).padStart(2, '0') + '-' + String(today.getDate()).padStart(2, '0');
        var time = String(today.getHours()).padStart(2, '0') + ":" + String(today.getMinutes()).padStart(2, '0') + ":" + String(today.getSeconds()).padStart(2, '0');
        var dateNow = date + 'T' + time + '.000Z';
        return dateNow;
    };
}
