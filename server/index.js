const path = require('path');
const express = require('express');
const app = express();
app.use(express.static(path.join(__dirname, "..", "public")));

const { PORT = 5000 } = process.env;

// Routing
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, '..', 'public', 'index.html'));
})

app.get('/cars', function(req, res) {
    res.sendFile(path.join(__dirname, '..', 'public', 'cars.html'));
})

// Menjalankan server
app.listen(PORT, '0.0.0.0', () => {
    console.log("Server sudah berjalan, silahkan buka http://0.0.0.0:%d", PORT);
})



